import type { NextPage } from "next";
import { useEffect } from "react";
import { useAppSelector } from "../app/hooks/redux";
import Header from "../app/components/Header";
import Loader from "../app/components/Loader";
import Error from "../app/components/Error";
import { useActions } from "../app/hooks/redux/useActions";
import axios from "../app/plugins/axios";
import { IBtcResponse, ICurrency } from "../app/models/currency";
import Main from "../app/components/Main";

interface HomeProps {
  currencies: ICurrency[];
}

const Home: NextPage<HomeProps> = ({ currencies: serverCurrencies }) => {
  const { fetchCurrency } = useActions();
  const { currencies, error, isLoading } = useAppSelector(
    (state) => state.btcReducer
  );

  useEffect(() => {
    const intervalUpdate: NodeJS.Timeout = setInterval(() => {
      fetchCurrency();
    }, 5000);

    return;
    clearInterval(intervalUpdate);
  }, []);
  return (
    <div className={"flex  flex-col min-h-screen"}>
      <Header />
      {isLoading ? (
        <Loader heightSubtract={48} />
      ) : error ? (
        <Error>{error}</Error>
      ) : (
        <Main currencies={currencies.length ? currencies : serverCurrencies} />
      )}
    </div>
  );
};

export default Home;

export async function getServerSideProps({}) {
  const response = await axios.get<IBtcResponse>(
    "https://api.coindesk.com/v1/bpi/currentprice.json"
  );
  return {
    props: { currencies: Object.values(response.data.bpi) },
  };
}
