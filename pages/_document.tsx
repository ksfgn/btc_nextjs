import { Head, Html, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html>
      <Head>
        <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
        <title>BTC Currency</title>
        <meta name="author" content="Oleksandr Shapovalov" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <body className={"bg-gray-50"}>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
