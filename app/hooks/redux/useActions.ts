import { bindActionCreators } from "redux";
import { btcActions } from "../../store/reducers/BTC/show_BTC_Slice";
import { useDispatch } from "react-redux";
import * as btcAsyncActions from "./../../store/reducers/BTC/actions";
const allActions = {
  ...btcActions,
  ...btcAsyncActions,
};

export const useActions = () => {
  const dispatch = useDispatch();

  return bindActionCreators(allActions, dispatch);
};
