import axios from "axios";

const instance = axios.create({
  baseURL: "https://api.coindesk.com/v1/bpi/currentprice.json",
});
export default instance;