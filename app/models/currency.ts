export interface ICurrency {
  code: Currencies;
  symbol: string;
  rate: string;
  description: string;
  rate_float: number;
}

export enum Currencies {
  USD = "USD",
  GBP = "GBP",
  EUR = "EUR",
}

export interface IBtcResponse {
  bpi: {
    [Currencies.EUR]: ICurrency;
    [Currencies.USD]: ICurrency;
    [Currencies.GBP]: ICurrency;
  };
}
