import React, { FC } from "react";

type ErrorProps = {
  children?: string;
};
const Error: FC<ErrorProps> = ({ children }) => {
  return (
    <div
      className={
        "flex items-center justify-center text-6xl text-red-700 flex-col"
      }
    >
      {children ? children : "Ooooops, something went wrong"}

      <p>: - (</p>
    </div>
  );
};

export default Error;
