import React, { FC } from "react";
import { decode } from "html-entities";
import { ICurrency } from "../models/currency";

interface MainProps {
  currencies: ICurrency[];
}

const Main: FC<MainProps> = ({ currencies }) => {
  return (
    <main
      className={
        "lg:container px-3  py-7 flex-auto flex items-center justify-center flex-col " +
        "text-center text-blue-50  mx-auto "
      }
    >
      <section
        className={
          "flex items-stretch justify-center gap-4 w-full flex-wrap  md:flex-nowrap"
        }
      >
        {currencies.map((el) => (
          <div
            key={el.code}
            className={
              "py-4 px-2.5 border-emerald-800 border " +
              "rounded-xl bg-lime-50 basis-full   sm:basis-4/12   " +
              " items-stretch flex flex-col grow shrink "
            }
          >
            <h2 className={"text-2xl text-emerald-900  flex-auto"}>
              {el.description}
            </h2>
            <h3 className={"rounded-full text-xl text-teal-800"}>
              1 BTC = {el.rate_float} <span>{decode(el.symbol)}</span>
            </h3>
          </div>
        ))}
      </section>
    </main>
  );
};

export default Main;
