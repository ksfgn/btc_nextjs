import React, { FC } from "react";

interface LoaderProps {
  heightSubtract: number;
}

const Loader: FC<LoaderProps> = ({ heightSubtract }) => {
  return (
    <div
      className={"flex items-center justify-center"}
      style={{ height: `calc(100vh - ${heightSubtract || 100}px)` }}
    >
      <div className="lds-ring">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default Loader;
