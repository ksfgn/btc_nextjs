import React, { FC } from "react";

const Header: FC = () => {
  return (
    <header
      className={
        "bg-emerald-800 h-12 px-3 text-2xl text-blue-50 flex items-center"
      }
    >
      <div> BTC Currency</div>
    </header>
  );
};

export default Header;
