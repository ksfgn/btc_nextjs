import { ICurrency } from "../../../models/currency";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { fetchCurrency } from "./actions";

export interface IBtcState {
  currencies: ICurrency[];
  error: string;
  isLoading: boolean;
}

const initialState: IBtcState = {
  currencies: [],
  error: "",
  isLoading: false,
};

export const btcSlice = createSlice({
  name: "BTC",
  initialState,
  reducers: {},
  extraReducers: {
    [fetchCurrency.pending.type]: (state, action: PayloadAction) => {
      //preloader showing
      // state.isLoading = true;
    },
    [fetchCurrency.fulfilled.type]: (
      state,
      action: PayloadAction<ICurrency[]>
    ) => {
      state.isLoading = false;
      state.error = "";
      state.currencies = action.payload;
    },
    [fetchCurrency.rejected.type]: (state, action: PayloadAction<string>) => {
      state.isLoading = false;
      state.error = action.payload;
    },
  },
});
//export const selectCount = (state: RootState) => state;

export const btcReducer = btcSlice.reducer;
export const btcActions = btcSlice.actions;
