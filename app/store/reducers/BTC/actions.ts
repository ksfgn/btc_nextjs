import { createAsyncThunk } from "@reduxjs/toolkit";
import { getErrorMessage } from "../../../utils/getErrorMessage";
import { IBtcResponse } from "../../../models/currency";
import axios from "../../../plugins/axios";

export const fetchCurrency = createAsyncThunk(
  "BTC/fetchAllCurrency",
  async (arg, thunkAPI) => {
    try {
      const response = await axios.get<IBtcResponse>("");
      return Object.values(response.data.bpi);
    } catch (e) {
      console.error(e);
      return thunkAPI.rejectWithValue(getErrorMessage(e));
    }
  }
);
