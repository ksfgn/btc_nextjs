import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";
import { btcReducer } from "./reducers/BTC/show_BTC_Slice";

const rootReducer = combineReducers({
  btcReducer,
});

export const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
